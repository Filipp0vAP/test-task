# test-task

## Installation
1. git clone https://gitlab.com/Filipp0vAP/test-task.git
2. cd test-task
3. docker compose up -d

## For Ansible
1. git clone https://gitlab.com/Filipp0vAP/test-task.git
2. cd test-task/ansible
3. add your hosts into inventory
4. ansible-playbook deploy_on_test.yml